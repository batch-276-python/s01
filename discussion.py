# [Section] Comments
# Comments in Python are done using "ctrl+/" or # symbol

# [Section] Python Syntax
# Hello World in Python
print("Hello World")

# [Section] Python Indentation Where in other programming langauges the indentaion in code is for readability only,
# the indentation in python is very important In Python, indentation is used to indicate a block of code. Similar to
# JS, there is no need to end statements with semicolons

# [Section] Variables
# Variables are the container of data
# In Python, a variable is declared by stating the variable name and assigning a value using the equality symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with a letter (A to Z or a to z), dollar sign or an underscore
# After the first character identifier can have any combination of characters
# Unlike JavaScript that uses the camel casing,
# Python uses the snake case convention for variables as defined in the PEP (Python Enhancement Proposal).
# Mostly, identifiers are case-sensitive.
age = 35
middle_initial = "C"

# Python allows assigning of values to multiple variables in one line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name1)

# Different ways of printing
# print(f"{name1} is the oldest")
# print(f"""{name1} is the oldest,
# {name2} is the second oldest,
# {name3} is the youngest""")
# print("%s is the oldest" % name1)
# print("{1} is the oldest and {0} is the second oldest.".format(name1, name2))
# print(name1, "is the oldest")

# [Section] Data Types
# Data types convey what kind of information a variable holds. There are different data types and each has its own use.
# Python has the following data types:

# 1. String(str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Number(int, float, complex) - for integers, decimals and complex numbers
num_of_days = 365  # this is an integer
pi_approx = 3.1416  # this is a float
complex_num = 1 + 5j  # this is a complex number

# type() function returns the data type of a variable
print(type(num_of_days))

# 3. Boolean(bool) - for Truthy and Falsy values
# Boolean values in Python start with a capital letter
isLearning = True
isDifficult = False
print(type(isLearning))

# [Section] Using Variables
# Just like in JS variables can be used by calling the name of the identifier or variable
print("My name is " + full_name)
print(f"My name is {full_name}")

# [Section] Terminal Outputs
# print() function is used to print to the terminal
# To use variables, concatenate (+ symbol) between the string and the variable
# However, we cannot concatenate a string and a number together or with different data types
# print("My age is " + age)  ## This will throw an error

# [Section] Typecasting
# Typecasting is the process of converting a variable from one data type to another

# here are some functions that can be use in typecasting
# int() - converts to integer
print(int(3.15))  # returns 3 only and remove the decimal

# float() - converts to float
print(float(3))  # returns 3.0

# str() - converts to string
print("My age is " + str(age))

# f-string - a new way of printing to the terminal
# another way to avoid typo error when concatenating strings and variables
print(f"My name is {full_name} and I am {age} years old")


# [Section] Operators
# Python has operator families that can be used to manipulate variables and data types

# 1. Arithmetic Operators - performs mathematical operations

print(5 + 3)  # addition
print(5 - 3)  # subtraction
print(5 * 3)  # multiplication
print(5 / 3)  # division
print(5 % 3)  # modulus
print(5 ** 3)  # exponent
print(5 // 3)  # floor division

# 2. Assignment Operators - assigns values to variables
num1 = 4
num1 += 3  # num1 = num1 + 3 # returns 7
# other assignment operators are -=, *=, /=, %=, **=, //=, &=, |=, ^=, >>=, <<=

# 3. Comparison Operators - compares values and returns a boolean
print(1 == 1)  # returns True
print(1 != 1)  # returns False
print(1 == "1")  # returns False
print(1 == int("1"))  # returns True
# other comparison operators are >, <, >=, <=, !=

# 4. Logical Operators - used to combine conditional statements
# and - returns True if both statements are True

# Logical OR operators - returns True if one of the statements is True
print(True or False)  # returns True

# Logical NOT operators - returns the opposite of the boolean value(
print(True and False)  # returns False
print(1 == 1 and 2 == 2)  # returns True

# Logical NOT operators - returns the opposite of the boolean value
print(not True)  # returns False

















