# Python Introduction(python setup, Data types)

## Python Libraries
### How to install a library
```shell
pip install <library>
```
### How to export installed libraries
```shell
pip freeze > requirements.txt
```
### How to install libraries from requirements.txt
```shell
pip install -r requirements.txt
```
### How to install a specific version of a library
```shell
pip install <library>==<version>
```
