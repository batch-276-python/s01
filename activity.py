# Activity: activity.py

name = "Ian"
age = 27
occupation = "Pirate"
movie = "One Piece"
rating = 10.0  # float - decimal number

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}")

num1 = 4
num2 = 3
num3 = 2

# Get the product of num1 and num2
print(num1 * num2)

# check if num1 is less than num3
print(num1 < num3)

# add the value of num3 to num2
print(num2 + num3)